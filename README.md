# const-h5

`const-cli`模板项目，此项目为`Html5`模板

``` shell
npm i const-cli -g
const init Project_Name h5
```

本模板集成了：

- vue.min.js
- jquery.min.js
- echarts.min.js

## 模板如何使用

``` shell
const init Project_Name h5
```

## LICENSE

[MIT](LICENSE)